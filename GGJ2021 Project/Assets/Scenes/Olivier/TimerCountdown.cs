using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerCountdown : MonoBehaviour
{

    public GameObject textDiplay;
    public int secondsLeft = 30;
    public bool takingAway;

    void Start()
    {
        textDiplay.GetComponent<Text>().text = "" + secondsLeft;
    }

    void Update()
    {
        if(takingAway == false && secondsLeft > 0)
        {
            StartCoroutine(TimerTake());
        }
        if(secondsLeft <= 0)
        {
            textDiplay.GetComponent<Text>().text = "GO!";
        }
    }
    IEnumerator TimerTake()
    {
        takingAway = true;
        yield return new WaitForSeconds(1);
        secondsLeft -= 1;
        textDiplay.GetComponent<Text>().text = "" + secondsLeft;
        takingAway = false;
    }
    

}
