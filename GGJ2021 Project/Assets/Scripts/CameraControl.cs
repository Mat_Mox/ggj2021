using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraControl : MonoBehaviour
{
    [SerializeField] private Transform cameraPivot = null;
    private Vector2 motion;

    public void OnLook(InputAction.CallbackContext context)
    {
        motion = context.ReadValue<Vector2>();
        cameraPivot.Rotate(0, motion.x /100* SettingsMenu1.MouseSensitivity, 0);
    }
}
