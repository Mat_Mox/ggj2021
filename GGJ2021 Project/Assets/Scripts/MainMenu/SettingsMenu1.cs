using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Linq;

public class SettingsMenu1 : MonoBehaviour
{
    public AudioMixer audioMixer;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;

    public Slider soundMusicSlider;

    public Toggle fullScreenToggle;

    public Slider mouseSensitivity;

    public void Start()
    {
        resolutions = Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct().ToArray();
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        //Screen.fullScreen = true;
        fullScreenToggle.isOn = FullScreenEnable;

        audioMixer.SetFloat("volume", SoundEffectVolume);

        soundMusicSlider.value = SoundEffectVolume;

        mouseSensitivity.value = MouseSensitivity;

    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
        SoundEffectVolume = volume;
    }

    public static float SoundEffectVolume
    {
        get { return PlayerPrefs.GetFloat("SoundEffectVolume", 0f); }
        set { PlayerPrefs.SetFloat("SoundEffectVolume", value); }
    }


    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
        FullScreenEnable = isFullScreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public static bool FullScreenEnable
    {
        get { return PlayerPrefs.GetInt("FullScreenEnable", 1) != 0; }
        set { PlayerPrefs.SetInt("FullScreenEnable", value ? 1 : 0); }
    }

    public static float MouseSensitivity
    {
        get { return PlayerPrefs.GetFloat("MouseSensitivity", 10f); }
        set { PlayerPrefs.SetFloat("MouseSensitivity", value); }
    }
    public void SetMouseSensitivity(float sensitivity)
    {
        MouseSensitivity = sensitivity;
    }
}
