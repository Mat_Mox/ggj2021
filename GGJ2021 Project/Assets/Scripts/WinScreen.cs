using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Mirror;
using TMPro;


public class WinScreen : NetworkBehaviour
{
    [HideInInspector] public float chrono = 10.0f;
    [SerializeField] private TextMeshProUGUI timer = null;
    [SerializeField] private TextMeshProUGUI winnerNameText;
    private FCGameManager gameManager;

    private void Start()
    {
        timer.SetText("Returning to lobby in " + Mathf.CeilToInt(chrono).ToString() + "s");
        DeactivateWinScreen();
        gameManager = FindObjectOfType<FCGameManager>();
    }

    private void Update()
    {
        if (gameManager.partyState == EPartyState.Ending)
        {
            if (chrono <= 0)
            {
                timer.SetText("Returning to lobby in 0s");
                // TODO tp player in lobby
            }
            else
            {
                chrono -= Time.deltaTime;
                timer.SetText("Returning to lobby in " + Mathf.CeilToInt(chrono).ToString() + "s");
            }
        }
        else
        {
            chrono = 10;
        }
    }

    public void Quit()
    {
        if (isServer)
        {
            NetworkServer.Shutdown();
        }
        NetworkClient.Disconnect();

        SceneManager.LoadScene("MainMenu");
    }

    public void SetWinnerNameText(string name)
    {
        winnerNameText.text = name + " wins!";
    }

    public void ActivateWinScreen()
    {
        GetComponent<Canvas>().enabled = true;
    }
    
    public void DeactivateWinScreen()
    {
        GetComponent<Canvas>().enabled = false;
    }
}
