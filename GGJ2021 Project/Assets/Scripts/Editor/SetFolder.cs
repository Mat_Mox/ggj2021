﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SetFolder : Editor
{
    [MenuItem("Tools/Create Folder")]

    static void CreateFolder()
    {
        #region Art
        if (!AssetDatabase.IsValidFolder("Assets/Art"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Art"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Animations"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Animations"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Materials"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Materials"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Shaders"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Shaders"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Sprites"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Sprites"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Textures"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Textures"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Fonts"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Fonts"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Art/Models"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Art", "Models"));
        }
        #endregion

        #region Functionals
        if (!AssetDatabase.IsValidFolder("Assets/Functionals"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Functionals"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Functionals/Animators"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Functionals", "Animators"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Functionals/Physics"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Functionals", "Physics"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Functionals/TileMaps"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Functionals", "TileMaps"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Functionals/Tiles"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Functionals", "Tiles"));
        }
        #endregion

        #region Scenes
        if (!AssetDatabase.IsValidFolder("Assets/Scenes"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Scenes"));
        }
        #endregion

        #region Sounds
        if (!AssetDatabase.IsValidFolder("Assets/Sounds"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Sounds"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Sounds/Music"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Sounds", "Music"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Sounds/SFX"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Sounds", "SFX"));
        }
        #endregion

        #region Scripts
        if (!AssetDatabase.IsValidFolder("Assets/Scripts"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Scripts"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Scripts/Editor"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Scripts", "Editor"));
        }
        if (!AssetDatabase.IsValidFolder("Assets/Scripts/MainMenu"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Scripts", "MainMenu"));
        }
        #endregion

        #region Prefabs
        if (!AssetDatabase.IsValidFolder("Assets/Prefabs"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Prefabs"));
        }
        #endregion

        #region Settings
        if (!AssetDatabase.IsValidFolder("Assets/Settings"))
        {
            AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets", "Settings"));
        }
        #endregion

        /*
        if (!System.IO.Directory.Exists(Application.dataPath + System.IO.Path.GetDirectoryName(assetPath).TrimStart("Assets".ToCharArray()) + "/Materials"))
        {
            AssetDatabase.CreateFolder(System.IO.Path.GetDirectoryName(assetPath), "Materials");
        }
        */
    }
}