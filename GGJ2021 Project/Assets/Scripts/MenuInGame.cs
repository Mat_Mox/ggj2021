using UnityEditor;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class MenuInGame : NetworkBehaviour
{
    private bool menuInGame = false;
    public Canvas MenuUI;
    
    public void ToggleMenu()
    {
        if (menuInGame)
        {
            Resume();
        }
        else
        {
            Menu();
        }
    }

    void Menu()
    { 
        MenuUI.enabled = true;
        menuInGame = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void Resume()
    {
        MenuUI.enabled = false;
        menuInGame = false;
        if (isServer && FindObjectOfType<FCGameManager>().partyState == EPartyState.Waiting)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    public void LoadMainMenu()
    {
        if (isServer)
        {
            NetworkServer.Shutdown();
        }
        NetworkClient.Disconnect();
        
        SceneManager.LoadScene("MainMenu");
    }
}
