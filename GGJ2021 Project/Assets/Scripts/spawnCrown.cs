using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class spawnCrown : NetworkBehaviour
{
    GameObject[] spawnPoints;
    GameObject currentPoint;
    int index;

    GameObject crown;

    private FCGameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<FCGameManager>();
        //SpawnCrown();
    }

    void Update()
    {
        if (isServer && (gameManager.partyState == EPartyState.Searching || gameManager.partyState == EPartyState.Bringing))
        {
            if (gameManager.crownOwner != null)
            {
                transform.position = new Vector3(gameManager.crownOwner.transform.position.x, 
                    gameManager.crownOwner.transform.position.y + 1f,
                    gameManager.crownOwner.transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            }
        }
        
    }

    public void SpawnCrown()
    {
        if (isServer)
        {
            spawnPoints = GameObject.FindGameObjectsWithTag("point");
            index = Random.Range(0, spawnPoints.Length);
            currentPoint = spawnPoints[index];

            crown = GameObject.FindGameObjectWithTag("Crown");
            crown.transform.position = currentPoint.transform.position;
        }
    }
    
    public void HideCrown()
    {
        if (isServer)
        {
            transform.position = new Vector3(0, -20, 0);
        }
    }
}
